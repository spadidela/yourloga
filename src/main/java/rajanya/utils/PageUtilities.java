package rajanya.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.MessageFormat;

public class PageUtilities {
    private static final Logger LOGGER = Logger.getLogger(PageUtilities.class);
    private WebDriver driver;

    public PageUtilities(WebDriver driver) {
        this.driver = driver;
    }

    public void waitForElement(WebElement webElement) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 5);
            wait.until(ExpectedConditions.visibilityOf(webElement));
        } catch (TimeoutException te) {
            LOGGER.info(te.getCause());
        }
    }

    public void sendKeys(final WebElement element, final String value) {

        try {
            waitForElement(element);
            element.clear();
            element.sendKeys(value);
        } catch (NoSuchElementException nse) {
            LOGGER.warn(MessageFormat.format("Failed to enter the value ", element), nse);
        }
    }

    public void selectFromDropDown(WebElement element, String selection) {
        element.click();
        Select listItem = new Select(element);
        listItem.selectByValue(selection);
    }

    public void waitForElementToBePresent(WebElement webElement) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 5);
            wait.until(ExpectedConditions.visibilityOf(webElement));
        } catch (TimeoutException te) {
            LOGGER.info(te.getCause());
        }
    }
    public void waitForElementToBeClickable(WebElement webElement) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
    }
}
