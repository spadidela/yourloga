package rajanya.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {

    @FindBy(xpath = "//a[contains(text() , 'Sign in')]")
    public WebElement button_signIn;


    @FindBy(id = "email")
    public WebElement emailAddress;

    @FindBy(id = "passwd")
    public WebElement password;

    @FindBy(id = "SubmitLogin")
    public WebElement submitBtn;

    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }


    public void clickOnSignInButton() {
        pageUtilities.waitForElementToBePresent(button_signIn);
        button_signIn.click();
    }

    public void insertValue(WebElement element, String value) {
        pageUtilities.sendKeys(element, value);
    }

    public void clickOnSubmitButton() {
        pageUtilities.waitForElementToBeClickable(submitBtn);
        submitBtn.click();
    }

}
