package rajanya.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import rajanya.utils.PageUtilities;

public class MyAccountPage extends BasePage {
    @FindBy(xpath = "//span[contains(.,'My personal information')]")
    public WebElement button_MyPersonalInfo;

    @FindBy(id = "firstname")
    public WebElement text_firstName;

    @FindBy(id = "old_passwd")
    public WebElement text_currentPassword;

    @FindBy(xpath = "//button[@name='submitIdentity']")
    public WebElement button_save;

    public MyAccountPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    public void clickOnPersonalInformation(){
        pageUtilities.waitForElementToBeClickable(button_MyPersonalInfo);
        button_MyPersonalInfo.click();
    }

    public void updateFirstName(String newVal,String password){
        insertValue(text_firstName,newVal);
        insertValue(text_currentPassword,password);
        button_save.click();


    }
    public void insertValue(WebElement element, String value) {
        pageUtilities.sendKeys(element, value);
    }
}
