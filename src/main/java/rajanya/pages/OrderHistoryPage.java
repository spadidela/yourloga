package rajanya.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OrderHistoryPage extends BasePage {
    @FindBy(xpath = "(//td[@class='history_link bold footable-first-column'])[1]")
    public WebElement orderRef;


    public OrderHistoryPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

}
