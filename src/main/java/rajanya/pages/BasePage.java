package rajanya.pages;

import org.openqa.selenium.WebDriver;
import rajanya.utils.PageUtilities;

public class BasePage {

    protected WebDriver driver;

    protected PageUtilities pageUtilities;

    public BasePage(WebDriver driver) {

        this.driver = driver;
        pageUtilities = new PageUtilities(driver);
    }


}
