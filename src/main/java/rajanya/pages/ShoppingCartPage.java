package rajanya.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShoppingCartPage  extends BasePage{


    @FindBy(xpath = "//span[contains(.,'Proceed to checkout')]")
    public WebElement button_proceedToCheckout;

    @FindBy(xpath = "(//span[contains(.,'Proceed to checkout')])[2]")
    public WebElement button_continueToCheckout;

    @FindBy(xpath = "//input[contains(@type,'checkbox')]")
    public WebElement check_termsAndConditions;

    public ShoppingCartPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }


    public void proceedWithCheckout(){
        pageUtilities.waitForElementToBeClickable(button_proceedToCheckout);
        button_proceedToCheckout.click();

        button_continueToCheckout.click();
        button_continueToCheckout.click();
        check_termsAndConditions.click();
        button_continueToCheckout.click();

    }
}
