package rajanya.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaymentPage extends BasePage{
    @FindBy(xpath = "//a[@title='Pay by bank wire']")
    public WebElement link_paymentType;

    @FindBy(xpath = "//span[contains(.,'I confirm my order')]")
    public WebElement button_confirmTheOrder;

    @FindBy(xpath = "//strong[@class='dark'][contains(.,'Your order on My Store is complete.')]")
    public WebElement text_orderConfirmation;

    @FindBy(xpath = "//div[contains(@class,'box')]")
    public WebElement text_referenceNumber;

    @FindBy(xpath = "//div[contains(@class,'box')]")
    public WebElement button_backToOrderHistory;

    public PaymentPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    public void makeAPayment(){
        pageUtilities.waitForElementToBeClickable(link_paymentType);
        link_paymentType.click();
    }

    public void confirmTheOrder(){
        pageUtilities.waitForElementToBeClickable(button_confirmTheOrder);
        button_confirmTheOrder.click();
    }

    public void verifyTheOrderIsConfirmed(){
        pageUtilities.waitForElementToBePresent(text_orderConfirmation);
    }

    public String getReference(){
        pageUtilities.waitForElementToBePresent(text_referenceNumber);
        return text_referenceNumber.getText().split("order reference")[1].split("in the subject of your bank wire")[0].trim();
    }

    public void navigateToOrderHistory(){
        pageUtilities.waitForElementToBeClickable(button_backToOrderHistory);
        button_backToOrderHistory.click();
    }
}
