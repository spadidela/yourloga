package rajanya.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddToCartPage extends BasePage{
    private Actions action;
    @FindBy(xpath ="(//a[contains(@title,'T-shirts')])[2]")
    public WebElement link_tShirts;

    @FindBy(xpath = "//img[@title='Faded Short Sleeve T-shirts']")
    public WebElement select_tShirt;

   @FindBy(xpath = "//span[contains(.,'Add to cart')]")
   public WebElement button_addToCart;

    @FindBy(xpath = "//h2[contains(.,'Product successfully added to your shopping cart')]")
    public WebElement text_addToCartSuccessfulMessage;



    public AddToCartPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
        action = new Actions(driver);
    }

    public void clickOnTShirtLink() {
        pageUtilities.waitForElementToBePresent(link_tShirts);
        link_tShirts.click();
    }

    public void selectATShirt() {
        pageUtilities.waitForElementToBePresent(select_tShirt);
        action.moveToElement(select_tShirt).perform();
    }
    public void clickAddToCartButton() {
        pageUtilities.waitForElementToBePresent(button_addToCart);
        action.moveToElement(button_addToCart).perform();
        button_addToCart.click();
    }

    public void verifyTheItemAddedToTheCart(){
        pageUtilities.waitForElementToBePresent(text_addToCartSuccessfulMessage);
    }

}
