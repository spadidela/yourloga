package scenario;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Priority;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import rajanya.pages.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

public class YourLogoTests {
    private WebDriver driver;
    private LoginPage loginPage;
    private AddToCartPage addToCartPage;
    private ShoppingCartPage shoppingCartPage;
    private PaymentPage paymentPage;
    private OrderHistoryPage orderHistoryPage;
    private MyAccountPage myAccountPage;

    @Before
    public void setup() throws IOException {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver 4");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(readProperties("appUrl"));
        loginPage = new LoginPage(driver);
        addToCartPage = new AddToCartPage(driver);
        shoppingCartPage = new ShoppingCartPage(driver);
        paymentPage = new PaymentPage(driver);
        orderHistoryPage = new OrderHistoryPage(driver);
        myAccountPage = new MyAccountPage(driver);
    }

    @Test
    public void OrderTShirt() throws IOException {
        login();
        addToCart();
        placeAOrder();
    }

    @Test
    public void updatePersonalInfo() throws IOException {
        login();
        myAccountPage.clickOnPersonalInformation();
        myAccountPage.updateFirstName("Rajanya"+ RandomStringUtils.randomAlphabetic(5),readProperties("password"));

    }



    private String readProperties(String key) throws IOException {
        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String appConfigPath = rootPath + "application.properties";
        Properties appProps = new Properties();
        appProps.load(new FileInputStream(appConfigPath));
        return appProps.getProperty(key);
    }

    private void login() throws IOException {
        loginPage.clickOnSignInButton();
        loginPage.insertValue(loginPage.emailAddress, readProperties("email"));
        loginPage.insertValue(loginPage.password, readProperties("password"));
        loginPage.clickOnSubmitButton();
    }

    private void addToCart() {
        addToCartPage.clickOnTShirtLink();
        addToCartPage.selectATShirt();
        addToCartPage.clickAddToCartButton();
        addToCartPage.verifyTheItemAddedToTheCart();
    }

    private void placeAOrder() {
        shoppingCartPage.proceedWithCheckout();
        paymentPage.makeAPayment();
        paymentPage.confirmTheOrder();
        paymentPage.verifyTheOrderIsConfirmed();
//        String expectedOrderRef=paymentPage.getReference();
        paymentPage.navigateToOrderHistory();
    }


    @After
    public void exitBrowser() {
        driver.quit();
    }
}
